# SeisComP3 install
export SEISCOMP_ROOT="/opt/seiscomp3/"
export LD_LIBRARY_PATH="$SEISCOMP_ROOT/lib:/usr/local/lib/x86_64-linux-gnu/:$LD_LIBRARY_PATH"
export PYTHONPATH="$PYTHONPATH:$SEISCOMP_ROOT/lib/python"
export MANPATH="$SEISCOMP_ROOT/man:$MANPATH"
export PATH="$SEISCOMP_ROOT/bin:$PATH"
source $SEISCOMP_ROOT/share/shell-completion/seiscomp.bash
seiscomp status
echo \$SEISCOMP_ROOT is $SEISCOMP_ROOT
