mysql -u root -p  seiscomp3 < /opt/seiscomp3/share/db/wfparam/mysql.sql

rsync -avzl cfg/seiscomp3.cnf  /etc/mysql/conf.d/
sudo service mysql restart || sudo /etc/init.d/mysql restart

rsync -avzl seiscomp@192.168.2.8:seiscomp3/etc/global.cfg $SEISCOMP_ROOT/etc/ 
rsync -avzl seiscomp@192.168.2.8:seiscomp3/etc/scmaster.cfg $SEISCOMP_ROOT/etc/
rsync -avzl seiscomp@192.168.2.8:seiscomp3/etc/scauto*3.cfg $SEISCOMP_ROOT/etc/
rsync -avzl seiscomp@192.168.2.8:seiscomp3/etc/scamp.cfg $SEISCOMP_ROOT/etc/
rsync -avzl seiscomp@192.168.2.8:seiscomp3/etc/scmag.cfg $SEISCOMP_ROOT/etc/
rsync -avzl seiscomp@192.168.2.8:seiscomp3/etc/scevent.cfg $SEISCOMP_ROOT/etc/
rsync -avzl seiscomp@192.168.2.8:.seiscomp3/key/  ~/.seiscomp3/key/
rsync -avzl seiscomp@192.168.2.8:.seiscomp3/licenses/  ~/.seiscomp3/licenses/
#rsync -avzl seiscomp@192.168.2.8:seiscomp3/share/scautoloc/ /opt/seiscomp3/share/scautoloc/

rsync -avzl cfg/*cfg $HOME/.seiscomp3/

seiscomp alias create scfd85sym scfinder
seiscomp alias create scfd20asym scfinder
seiscomp alias create scfdcrust scfinder
seiscomp alias create scautopic3 scautopick
seiscomp alias create scautoloc3 scautoloc
seiscomp alias create scvsmaglo2 scvsmaglog

rsync -avzl sysop@165.98.224.44:seiscomp3/share/FinDer/ /opt/seiscomp3/share/FinDer/
sed -i 's;home/sysop/seiscomp3;opt/seiscomp3;'  /opt/seiscomp3/share/FinDer/config/*config
ls /opt/seiscomp3/share/FinDer/config/*config | while read F ; do grep MAG_REGRESSION_THRESH $F || echo "MAG_REGRESSION_THRESH 5.5" >> $F ; done

seiscomp enable scfdcrust scfd85sym scfd20asym scautopic3 scautoloc3 scamp scmag scvsmag scenvelope scvsmaglog scvsmaglo2

mkdir ~/.seiscomp3/osm/
cd ~/.seiscomp3/osm/
perl ~/deployment/downloadosmtiles.pl --lat=-90:90 --lon=-180:180 --zoom=0:6 --baseurl='http://server.arcgisonline.com/arcgis/rest/services/World_Topo_Map/MapServer/tile/' 
perl ~/deployment/downloadosmtiles.pl --lat=13:14 --lon=-90:-88 --zoom=0:13 --baseurl='http://server.arcgisonline.com/arcgis/rest/services/World_Topo_Map/MapServer/tile/'  
find /home/marn/.seiscomp3/osm/ -type f |while read F; do convert $F $F;done
