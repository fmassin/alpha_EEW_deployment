### THE ROOT PASSWORD FOR MYSQL IS "root" (NO ")
MAINTAINER="Fred Massin  <fmassin@sed.ethz.ch>"
WORK_DIR=${HOME}/deployment/src/
INSTALL_DIR=/opt/seiscomp3/

mkdir $INSTALL_DIR 

apt-get update \
    && apt-get dist-upgrade -y --no-install-recommends \
    && apt-get install -y \
    build-essential \
    festival \
    cmake \
    cmake-curses-gui \
    flex \
    g++ \
    libgfortran3 \
    libncurses5 \
    libncurses5-dev \
    gfortran \
    git \
    libboost-dev \
    libboost-filesystem-dev \
    libboost-iostreams-dev \
    libboost-program-options-dev \
    libboost-regex-dev \
    libboost-thread-dev \
    libboost-system-dev \
    libboost-signals-dev \
    libboost-all-dev \
    python \
    python2.7 \
    python-dbus-dev \
    python-libxml2 \
    python-numpy \
    python-m2crypto \
    libghc-opengl-dev \
    libqt4-opengl-dev \
    libqtwebkit-dev \
    libmysqlclient-dev \
    mysql-server \
    cmake-qt-gui \
    libcdio-dev \
    libpython2.7 \
    libpython2.7-dev \
    libqt4-dev \
    libsqlite3-dev \
    sqlite3 \
    libpq5 \
    libpq-dev \
    python-twisted \
    libxml2 \
    libxml2-dev \
    openssh-server \
    openssl \
    libssl-dev \
    net-tools \
    vim \
    libfaketime \
    tmux \
    unzip \
    gmt \
    libgmt-dev \
    python-dateutil \
    python-stompy \
    activemq \
    libgeo-osm-tiles-perl


# install 5.2.1
rsync -avzl sysop@165.98.224.44:Documents/installations/gmt-5.2.1/ $WORK_DIR/gmt-5.2.1/ \
    && rm /home/marn/deployment/src/gmt-5.2.1/build/CMakeCache.txt \
    && cd $WORK_DIR/gmt-5.2.1/build/ \
    && cmake .. \
    && make \
    && make install

# install FInDer dependency : opencv 3.1.0 (cf https://github.com/milq/milq/blob/master/scripts/bash/install-opencv.sh)
OPENCV_VERSION='3.1.0'
rm -r $WORK_DIR/${OPENCV_VERSION}.zip $WORK_DIR/opencv-${OPENCV_VERSION}
cd $WORK_DIR/ \
    && wget  https://github.com/opencv/opencv/archive/${OPENCV_VERSION}.zip  \
    && unzip ${OPENCV_VERSION}.zip \
    && rm ${OPENCV_VERSION}.zip \
    && cd $WORK_DIR/opencv-${OPENCV_VERSION} \
    && mkdir build \
    && cd $WORK_DIR/opencv-${OPENCV_VERSION}/build \
    && cmake -DWITH_QT=ON -DWITH_OPENGL=ON -DFORCE_VTK=ON -DWITH_TBB=ON -DWITH_GDAL=ON -DWITH_XINE=ON -DBUILD_EXAMPLES=ON -DENABLE_PRECOMPILED_HEADERS=OFF .. \
    && make -j4 \
    && make install 

# install Finder
rm -r $WORK_DIR/FinDer
git clone https://gitlab.seismo.ethz.ch/SED-EEW/FinDer.git $WORK_DIR/FinDer \
    && cd $WORK_DIR/FinDer \
    && git pull \
    && cd $WORK_DIR/FinDer/libsrc/ \
    && make clean \
    && make install \
    && cd $WORK_DIR/FinDer/finder_file/ \
    && make \
    && make test 

# Install seiscomp
rm -r $WORK_DIR/seiscomp3
git clone https://github.com/SeisComP3/seiscomp3.git $WORK_DIR/seiscomp3 \
    && mkdir -p $WORK_DIR/seiscomp3/build \
    && cd $WORK_DIR/seiscomp3 \
    && git checkout release/jakarta \
    && git pull \
    && git submodule add -f https://gitlab.seismo.ethz.ch/SED-EEW/sed-addons.git src/sed-addons \
    && cd $WORK_DIR/seiscomp3/build \
    && cmake .. -DSC_GLOBAL_GUI=ON \
       -DSC_TRUNK_DB_MYSQL=ON -DSC_TRUNK_DB_POSTGRESQL=ON -DSC_TRUNK_DB_SQLITE3=ON \
       -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR \
       -DFinDer_INCLUDE_DIR=/usr/local/include/finder \
       -DFinDer_LIBRARY=/usr/local/lib/libFinder.a \
    && make -j $(grep -c processor /proc/cpuinfo) \
    && make install

# Environment config for seiscomp
chown -R $USER  $INSTALL_DIR
grep SEISCOMP_ROOT ~/.profile || cat cfg/seiscomp.profile >> ~/.profile
